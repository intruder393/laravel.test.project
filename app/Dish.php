<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{

    protected $table = 'dishs';

    public function ingredient() {
        //return $this->morphMany('Ingredient', 'dishs_ingredients');
        return $this->belongsToMany('\App\Ingredient', 'dishs_ingredients');
    }

    /*public function orderDish() {
        return $this->hasMany('\App\OrderDish');
    }*/

    public function orders() {
        return $this->belongsToMany('\App\Order', 'orders_dishs')->withPivot('count');
    }

    /*public function dishs() {
        return $this->belongsToMany('\App\Dish', 'orders_dishs')
            ->withPivot('count');
            //->selectRaw('dishs.* , sum(orders_dishs.count) as pivot_count')
            //->groupBy('dishs.id');
    }*/
}
