<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waiter extends Model {

    protected $table = 'waiters';

    public function orders() {
        return $this->hasMany(\App\Order::class);
    }
}
