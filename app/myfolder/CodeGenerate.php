<?php
namespace App\myfolder;

class CodeGenerator {

    public static $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890';
    public static $length = 10;
    public static $chars_length = 20;
    public static $gString = '';


    public static function get_rand_str() {
        //получаем длинну строки символов
        self::$chars_length = strlen(self::$chars) - 1;

        $string = self::$chars{rand(0,self::$chars_length)};
        for($i=1; $i < self::$length; $i++) {
            // Берем случайный элемент из набора символов
            $string .= self::get_rand_char($string, $i, self::$chars_length);
        }
        //echo "STRING > ".$string."<br>";
        self::$gString = $string;
        return $string;
    }

    public static function get_rand_char($string, $i, $length) {
        $rand = static::$chars{rand(0,$length)};
        if($rand != $string{$i-1}) {
            return $rand;
        } else {
            return static::get_rand_char($string, $i, $length);
        }
    }

    public static function getGuid(){
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 10);

        return $uuid;
    }
}