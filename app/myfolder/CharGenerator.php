<?php
namespace App\myfolder;

class CharGenerator extends CodeGenerator {

    public static $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ';
    public static $length = 10;
    public static $chars_length = 20;

    public static function get_chars_code() {
        $string = ( empty(parent::$gString) ) ? parent::get_rand_str() : parent::$gString;

        self::$chars = preg_replace("/[0-9]/", '', $string);
        self::$chars_length = strlen(self::$chars) - 1;

        $result = self::$chars{rand(0,self::$chars_length)};

        for($i=1; $i < self::$length; $i++) {
            // Берем случайный элемент из набора символов
            $result .= self::get_rand_char($result, $i, self::$chars_length);
        }
        //echo "CHAR_CODE > ".$result."<br>";
        return $result;
    }
}