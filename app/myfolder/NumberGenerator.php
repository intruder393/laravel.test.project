<?php
namespace App\myfolder;

class NumberGenerator extends CodeGenerator {

    public static $chars = '1234567890';
    public static $length = 10;
    public static $chars_length = 20;

    public static function get_number_code() {
        $result = '';
        $string = ( empty(parent::$gString) ) ? parent::get_rand_str() : parent::$gString;
        $numbers = preg_replace("/[^0-9]/", '', $string);

        self::$chars = ( !empty($numbers) && $numbers != '0' ) ? $numbers : self::$chars;
        self::$chars_length = strlen(self::$chars) - 1;

        for($i=0; $i < self::$length; $i++) {
            // Берем случайный элемент из набора символов
            $result .= self::$chars{rand(0,self::$chars_length)};
        }
        //echo "INT_CODE > ".$result."<br>";
        return $result;
    }
}
