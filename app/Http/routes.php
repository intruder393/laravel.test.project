<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('/task-1', 'HomeController@task1');
Route::get('/task-1/generator', 'HomeController@generator');

Route::get('/task-2', 'HomeController@task2');

Route::get('/task-3', 'HomeController@task3');
Route::get('/task-3/gettotalprofit', 'HomeController@getTotalProfit');
Route::get('/task-3/getlastorders', 'HomeController@getLastOrders');
Route::get('/task-3/gettopdishs', 'HomeController@getTopDishs');
