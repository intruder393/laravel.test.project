<?php

namespace App\Http\Controllers;

use App\Http\Requests;
//use App\myfolder\CodeGenerator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function __construct() {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        //$dishs = \App\Dish::all();
        //dd($dishs);

        //$order = \App\Order::all()->where();
        //$order = \App\OrderDish::groupBy('dish_id')->orderBy('sum', 'DESC')->selectRaw('*, sum(count) as sum')->get();
        //dd($order);
        //$orders = Turn::find(1)->orders;
        /*foreach($order->dishs as $item) {
            echo $item->pivot->count."<br>";
        }*/

        return view('home');
    }

    public function task2() {
        $result = array();

        $dishs = \App\Dish::all();
        $orders = \App\Order::all();
        $dishsInOrders = \App\Order::with('dish')->get();
        $topDish = \App\OrderDish::groupBy('dish_id')->orderBy('sum', 'DESC')->selectRaw('*, sum(count) as sum')->with('dishs')->take(5)->get();
        $topWaiter = \App\Order::groupBy('waiter_id')->orderby('count', 'DESC')->selectRaw('*, count(waiter_id) as count')->with('waiters')->get();


        $i=0;
        foreach($dishs as $item) {
            $result['dishs'][$i]['id']          = $item->id;
            $result['dishs'][$i]['title']       = $item->title;
            $result['dishs'][$i]['ingredients'] = '';
            $result['dishs'][$i]['price']       = $item->price;
            foreach($item->ingredient as $ingr) {
                $result['dishs'][$i]['ingredients'] .= ( !empty($result['dishs'][$i]['ingredients']) ) ? ', '.$ingr->title : $ingr->title;
            }

            $i++;
        }

        $i=0;
        foreach($orders as $item) {
            $result['orders'][$i]['id']    = $item->id;
            $result['orders'][$i]['date']  = $item->time_of_filing;
            $result['orders'][$i]['price'] = $item->price;
            $i++;
        }

        $i=0;
        foreach($dishsInOrders as $item) {
            $result['dio'][$i]['id']    = $item->id;
            $result['dio'][$i]['date']  = $item->time_of_filing;
            $result['dio'][$i]['dishs'] = '';
            foreach($item->dish as $dish) {
                $result['dio'][$i]['dishs'] .= ( !empty($result['dio'][$i]['dishs']) ) ? ', '.$dish->title : $dish->title;
            }
            $i++;
        }

        $i=0;
        foreach($topDish as $item) {
            $result['topDish'][$i]['id']    = $item->id;
            $result['topDish'][$i]['total'] = $item->sum;
            $result['topDish'][$i]['title'] = $item->dishs->title;

            $i++;
        }

        $i=0;
        foreach($topWaiter as $item) {
            $result['topWaiter'][$i]['name'] = $item->waiters->first_name.' '.$item->waiters->last_name;
            $result['topWaiter'][$i]['total'] = $item->count;
            $i++;
        }

        //dd($result);
        return view('task2', ['results' => $result]);
    }

    public function task3() {

        return view('task3');
    }

    public function getTotalProfit() {
        $total = \App\Order::whereBetween('time_of_filing', array(Carbon::now(), Carbon::now()->addDay()))
            ->sum('price');
        return json_encode(array('total' => $total));
    }

    public function getLastOrders() {
        $result = array();
        $orders = \App\Order::orderBy('id', 'DESC')->take(5)->get();
        $i=0;
        foreach($orders as $item) {
            $result[$i]['id']    = $item->id;
            $result[$i]['date']  = $item->time_of_filing;
            $result[$i]['price'] = $item->price;
            $i++;
        }
        return json_encode($result);
    }

    public function getTopDishs() {
        $result = array();
        $topDish = \App\OrderDish::groupBy('dish_id')->orderBy('sum', 'DESC')->selectRaw('*, sum(count) as sum')->with('dishs')->take(5)->get();
        $i=0;
        foreach($topDish as $item) {
            $result[$i]['id']    = $item->id;
            $result[$i]['total'] = $item->sum;
            $result[$i]['title'] = $item->dishs->title;
            $i++;
        }
        return json_encode($result);
    }

    public function task1() {
        $result['code'] = \App\myfolder\CodeGenerator::get_rand_str();
        $result['charCode'] = \App\myfolder\CharGenerator::get_chars_code();
        $result['numberCode'] = \App\myfolder\NumberGenerator::get_number_code();

        return view('task1', ['results' => $result]);
    }

    public function generator() {
        $result['code'] = \App\myfolder\CodeGenerator::get_rand_str();
        $result['charCode'] = \App\myfolder\CharGenerator::get_chars_code();
        $result['numberCode'] = \App\myfolder\NumberGenerator::get_number_code();

        return json_encode($result);
    }

}
