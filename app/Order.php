<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = 'orders';

    public function waiters() {
        return $this->belongsTo('\App\Waiter', 'waiter_id');
    }

    /*public function orderDish() {
        return $this->hasMany('\App\OrderDish');
    }*/

    public function dish() {
        return $this->belongsToMany('\App\Dish', 'orders_dishs')
            ->withPivot('count');
            //->selectRaw('dishs.* , sum(orders_dishs.count) as pivot_count')
            //->groupBy('dishs.id');
    }
}
