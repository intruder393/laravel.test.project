<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishIngredient extends Model {

    protected $table = 'dishs_ingredients';

    public function dishIngredient() {
        return $this->morphTo();
    }
}
