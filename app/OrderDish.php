<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDish extends Model {

    protected $table = 'orders_dishs';

    /*public function orders() {
        return $this->belongsTo('\App\Order', 'id');
    }*/

    public function dishs() {
        return $this->belongsTo('\App\Dish', 'dish_id');
    }
}
