<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model {

    protected $table = 'ingredients';

    public function dish() {
        //return $this->morphMany('Dish', 'dishs_ingredients');
        return $this->belongsToMany('\App\Dish', 'dishs_ingredients');
    }
}
