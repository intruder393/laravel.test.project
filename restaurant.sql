-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: restaurant
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dishs`
--

DROP TABLE IF EXISTS `dishs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `price` int(5) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishs`
--

LOCK TABLES `dishs` WRITE;
/*!40000 ALTER TABLE `dishs` DISABLE KEYS */;
INSERT INTO `dishs` VALUES (1,'ДАККУАЗ С КЛУБНИКОЙ',45,1),(2,'ЗАПЕЧЕННЫЕ ОВОЩИ «РАТАТУЙ» С ОСТРОЙ ТЕЛЯТИНОЙ',70,1),(3,'ФРИКАСЕ ИЗ КУРИЦЫ В ТОМАТЕ',50,1),(4,'МЯСНОЙ РУЛЕТ ПО-СРЕДИЗЕМНОМОРСКИ',68,1),(5,'ТОСКАНСКИЙ СУП ИЗ БЕЛОЙ ФАСОЛИ С ЧЕСНОЧНЫМИ ГРЕНКАМИ',65,1),(6,'ТОМАТНЫЙ КРЕМ-СУП С ХРУСТЯЩИМ РИСОМ',53,1),(7,'ЗЕЛЕНЫЙ БОРЩ',40,1),(8,'СЛИВОЧНО-СЫРНЫЙ СУП С ШАМПИНЬОНАМИ',55,1),(9,'ЗЕЛЕНЫЙ КРЕМ-СУП С КРАБОМ',70,1),(10,'РЫБНЫЙ СУП ИЗ ЛОСОСЯ',83,1),(11,'ПИЦЦА С ПАРМЕЗАНОМ,МИДИЯМИ И ГРИБАМИ',64,1),(12,'ПИЦЦА РУСТИКА',67,1),(13,'ПИЦЦА ИЗ ЛАВАША С КРЕВЕТКАМИ, АРТИШОКАМИ И МОЦАРЕЛЛОЙ',80,1),(14,'ШОКОЛАДНЫЙ МУСС С МАЛИНОЙ',30,1);
/*!40000 ALTER TABLE `dishs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishs_ingredients`
--

DROP TABLE IF EXISTS `dishs_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishs_ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dish_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dish_id` (`dish_id`),
  KEY `ingredient_id` (`ingredient_id`),
  CONSTRAINT `dishs_ingredients_ibfk_1` FOREIGN KEY (`dish_id`) REFERENCES `dishs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dishs_ingredients_ibfk_2` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishs_ingredients`
--

LOCK TABLES `dishs_ingredients` WRITE;
/*!40000 ALTER TABLE `dishs_ingredients` DISABLE KEYS */;
INSERT INTO `dishs_ingredients` VALUES (1,2,4),(2,2,1),(3,2,2),(4,2,3),(5,2,5),(6,3,6),(7,3,23),(8,3,7),(9,3,8),(10,3,10),(11,3,9),(12,4,11),(13,4,23),(14,4,12),(15,4,13),(16,4,14),(17,5,15),(18,5,16),(19,5,6),(20,5,17),(21,5,4),(22,6,23),(23,6,6),(24,6,8),(25,6,4),(26,6,18),(27,6,19),(28,6,20),(29,7,22),(30,7,23),(31,7,20),(32,7,21),(33,8,20),(34,8,24),(35,8,23),(36,8,6),(37,8,25),(38,9,22),(39,9,23),(40,9,4),(41,9,13),(42,9,26),(43,10,23),(44,10,17),(45,10,6),(46,10,27),(47,10,20),(48,10,28),(49,11,30),(50,11,29),(51,11,25),(52,11,22),(53,12,31),(54,12,30),(55,12,32),(56,12,13),(57,12,33),(58,13,34),(59,13,11),(60,13,4),(61,14,36),(62,14,37),(63,14,13),(64,1,38),(65,1,39),(66,1,37),(67,1,41),(68,1,13);
/*!40000 ALTER TABLE `dishs_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,'баклажаны'),(2,'помидоры'),(3,'перец болгарский'),(4,'чеснок'),(5,'телятина'),(6,'морковь'),(7,'сельдерей корень'),(8,'помидоры консервированные в собственном соку'),(9,'куриные бедра'),(10,'оливки зеленые'),(11,'моцарелла'),(12,'маслины'),(13,'яйца куриные'),(14,'говяжий фарш'),(15,'фасоль белая'),(16,'чиабатта '),(17,'сельдерей стебли'),(18,'рис'),(19,'бекон'),(20,'картофель'),(21,'щавель'),(22,'лук зеленый'),(23,'лук репчатый'),(24,'сыр'),(25,'шампиньоны'),(26,'крабовое мясо'),(27,'лосось филе'),(28,'лук-порей'),(29,'мидии варенные'),(30,'сыр пармезан'),(31,'рикотта'),(32,'ветчина'),(33,'шпинат'),(34,'креветки'),(36,'черный шоколад '),(37,'малина'),(38,'сыр маскарпоне'),(39,'молоко сгущенное'),(41,'фундук');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `waiter_id` int(11) NOT NULL,
  `time_of_filing` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `waiter_id` (`waiter_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`waiter_id`) REFERENCES `waiters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,'2016-06-25 21:37:51',206),(2,2,'2016-06-25 21:40:57',241),(3,3,'2016-06-25 08:27:26',257),(4,2,'2016-06-25 21:50:12',553),(5,5,'2016-06-25 21:57:31',282),(6,1,'2016-06-27 07:00:09',75),(7,1,'2016-06-25 22:09:59',256),(8,6,'2016-06-27 07:13:07',147);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_dishs`
--

DROP TABLE IF EXISTS `orders_dishs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_dishs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  `count` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `dish_id` (`dish_id`),
  CONSTRAINT `orders_dishs_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orders_dishs_ibfk_2` FOREIGN KEY (`dish_id`) REFERENCES `dishs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_dishs`
--

LOCK TABLES `orders_dishs` WRITE;
/*!40000 ALTER TABLE `orders_dishs` DISABLE KEYS */;
INSERT INTO `orders_dishs` VALUES (1,1,7,1),(2,1,4,2),(3,1,14,1),(4,2,14,1),(5,2,11,1),(6,2,12,1),(7,2,13,1),(8,3,1,1),(9,3,3,1),(10,3,4,1),(11,3,14,1),(12,3,11,1),(13,4,1,1),(14,4,2,1),(15,4,10,1),(16,4,11,2),(17,4,12,1),(18,4,13,2),(19,5,5,1),(20,5,7,1),(21,5,6,1),(22,5,14,2),(23,5,11,1),(24,6,1,1),(25,6,14,1),(26,7,1,2),(27,7,10,2),(28,8,12,1),(29,8,13,1);
/*!40000 ALTER TABLE `orders_dishs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waiters`
--

DROP TABLE IF EXISTS `waiters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waiters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waiters`
--

LOCK TABLES `waiters` WRITE;
/*!40000 ALTER TABLE `waiters` DISABLE KEYS */;
INSERT INTO `waiters` VALUES (1,'Владимир','Ардашов'),(2,'Анна','Ананикова'),(3,'Максим','Чехов'),(4,'Карина','Вовк'),(5,'Полина','Куликова'),(6,'Марина','Бессонова');
/*!40000 ALTER TABLE `waiters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-27 12:38:07
