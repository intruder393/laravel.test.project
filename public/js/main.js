jQuery(document).ready(function(){

    function tab2shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/gettotalprofit',
            dataType: 'json',
            success: function(data) {
                $('#total-profit').empty().append(data['total']);
            },
            error: function() {
                alert('Error!');
            }
        });
    }

    function tab3shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/getlastorders',
            dataType: 'json',
            success: function(data) {
                var tr = '';
                for (var item in data) {
                    tr += '<tr><th class="row">'+data[item]['id']+'</th><td>'+data[item]['date']+'</td><td>'+data[item]['price']+'</td></tr>';
                }
                $('#tab-3-table-main-tr').nextAll().remove();
                $('#tab-3-table-main-tr').after(tr);
            },
            error: function() {
                alert('Error!');
            }
        });
    }

    function tab4shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/gettopdishs',
            dataType: 'json',
            success: function(data){
                var tr = '';
                for (var item in data) {
                    tr += '<tr><th class="row">'+data[item]['id']+'</th><td>'+data[item]['total']+'</td><td>'+data[item]['title']+'</td></tr>';
                }
                $('#tab-4-table-main-tr').nextAll().remove();
                $('#tab-4-table-main-tr').after(tr);

            },
            error: function() {
                alert('Error!');
            }
        });
    }

    $('#t2').on('shown.bs.tab', function (e) {
        tab2shown();

    });

    $('#t3').on('shown.bs.tab', function (e) {
        tab3shown();
    });

    $('#t4').on('shown.bs.tab', function (e) {
        tab4shown();
    });

    $('.button-update').on('click', function(e) {
        var id = $(this).parent().attr('id');
        switch (id) {
            case 'tab-2': tab2shown(); break;
            case 'tab-3': tab3shown(); break;
            case 'tab-4': tab4shown(); break;
        }
    });

    $('#button-generator').on('click', function() {
        $.ajax({
            type: "GET",
            url: 'task-1/generator',
            dataType: 'json',
            success: function(data){
                var p = '';
                p += '<p>CODE >>>'+data['code']+'</p>';
                p += '<p>CHAR_CODE >>> '+data['charCode']+'</p>';
                p += '<p>NUM_CODE >>> '+data['numberCode']+'</p>';

                $('.panel-code-block').empty().append(p);

            },
            error: function() {
                alert('Error!');
            }
        });
    });

});