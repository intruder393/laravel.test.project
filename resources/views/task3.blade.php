@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">

                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" id="t1" data-toggle="tab">Мини описание</a></li>
                        <li><a href="#tab-2" id="t2" data-toggle="tab">Сумма заработанных денег за сегодня</a></li>
                        <li><a href="#tab-3" id="t3" data-toggle="tab">Список последних 5 заказов</a></li>
                        <li><a href="#tab-4" id="t4" data-toggle="tab">5 самых популярных блюд</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- *********TAB-1******** -->
                        <div class="tab-pane fade in active" id="tab-1">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Ajax запросы</div>

                                <div class="panel-body">
                                    <pre>
    function tab2shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/gettotalprofit',
            dataType: 'json',
            success: function(data) {
                $('#total-profit').empty().append(data['total']);
            },
            error: function() {
                alert('Error!');
            }
        });
    }

    function tab3shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/getlastorders',
            dataType: 'json',
            success: function(data) {
                var tr = '';
                for (var item in data) {
                    tr += '<tr><th class="row">'+data[item]['id']+'</th><td>'+data[item]['date']+'</td><td>'+data[item]['price']+'</td></tr>';
                }
                $('#tab-3-table-main-tr').nextAll().remove();
                $('#tab-3-table-main-tr').after(tr);
            },
            error: function() {
                alert('Error!');
            }
        });
    }

    function tab4shown() {
        $.ajax({
            type: "GET",
            url: 'task-3/gettopdishs',
            dataType: 'json',
            success: function(data){
                var tr = '';
                for (var item in data) {
                    tr += '<tr><th class="row">'+data[item]['id']+'</th><td>'+data[item]['total']+'</td><td>'+data[item]['title']+'</td></tr>';
                }
                $('#tab-4-table-main-tr').nextAll().remove();
                $('#tab-4-table-main-tr').after(tr);

            },
            error: function() {
                alert('Error!');
            }
        });
    }
                                    </pre>
                                </div>
                            </div>
                        </div>

                        <!-- *********TAB-2******** -->
                        <div class="tab-pane fade" id="tab-2">
                            <button type="button" id="button-update" class="btn btn-primary button-update">Обновить</button>
                            <h3>Общий доход за сегодня: <span id="total-profit" class="label label-default">0 </span>$</h3>
                        </div>

                        <!-- *********TAB-3******** -->
                        <div class="tab-pane fade" id="tab-3">
                            <button type="button" id="button-update" class="btn btn-primary button-update">Обновить</button>
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <!-- Table -->
                                <table class="table">
                                    <tr id="tab-3-table-main-tr">
                                        <th>#</th>
                                        <th>date</th>
                                        <th>price</th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <!-- *********TAB-4******** -->
                        <div class="tab-pane fade" id="tab-4">
                            <button type="button" id="button-update" class="btn btn-primary button-update">Обновить</button>
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <!-- Table -->
                                <table class="table">
                                    <tr id="tab-4-table-main-tr">
                                        <th>#</th>
                                        <th>total</th>
                                        <th>title</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
