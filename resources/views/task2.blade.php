@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h4> Task № 2 </h4>
                <div class="thumbnail">
                    <div class="caption">
                        <p>Структура БД для хранения информации имеет следующий вид:</p>
                    </div>
                    <img src="/public/image/schema.png">
                    <div class="caption">
                        <p>За основу был взят фреймворк Laravel. Работа с бд была организована
                        с помощью ORM Eloquent. SQL запросы прилогаются к каждой таблице.</p>
                    </div>
                </div>

                <!-- **************START****************** -->
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><b>Список блюд</b></div>
                    <div class="panel-body">
                        <pre>
SELECT d.title, GROUP_CONCAT(i.title SEPARATOR ', ') ingr_title, d.price FROM dishs d
	JOIN dishs_ingredients di ON di.dish_id = d.id
	JOIN ingredients i ON di.ingredient_id = i.id
	GROUP BY di.dish_id
	ORDER BY d.title;
                        </pre>
                    </div>

                    <!-- Table -->
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>title</th>
                            <th>ingredients</th>
                            <th>price</th>
                        </tr>
                        @foreach($results['dishs'] as $item )
                        <tr>
                            <th class="row">{{$item['id']}}</th>
                            <td>{{$item['title']}}</td>
                            <td>{{$item['ingredients']}}</td>
                            <td>{{$item['price']}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- **************END****************** -->

                <!-- **************START****************** -->
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><b>Список заказов</b></div>
                    <div class="panel-body">
                        <pre>
SELECT id, time_of_filing, price FROM orders
	ORDER BY id;
                        </pre>
                    </div>

                    <!-- Table -->
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>date</th>
                            <th>price</th>
                        </tr>
                        @foreach($results['orders'] as $item )
                            <tr>
                                <th class="row">{{$item['id']}}</th>
                                <td>{{$item['date']}}</td>
                                <td>{{$item['price']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- **************END****************** -->

                <!-- **************START****************** -->
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><b>Список блюд в заказе</b></div>
                    <div class="panel-body">
                        <pre>
SELECT o.id, o.time_of_filing, o.price, GROUP_CONCAT(d.title SEPARATOR ', ') dish_title FROM orders o
	JOIN orders_dishs od ON o.id = od.order_id
	JOIN dishs d ON od.dish_id = d.id
	GROUP BY o.id;
                        </pre>
                    </div>

                    <!-- Table -->
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>date</th>
                            <th>dishs</th>
                        </tr>
                        @foreach($results['dio'] as $item )
                            <tr>
                                <th class="row">{{$item['id']}}</th>
                                <td>{{$item['date']}}</td>
                                <td>{{$item['dishs']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- **************END****************** -->

                <!-- **************START****************** -->
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><b>Список 5 популярных блюд</b></div>
                    <div class="panel-body">
                        <pre>
SELECT d.title, sum(od.count) popular FROM orders_dishs od
	JOIN dishs d ON od.dish_id = d.id
	GROUP BY od.dish_id
	ORDER BY popular DESC
	LIMIT 5;
                        </pre>
                    </div>

                    <!-- Table -->
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>total</th>
                            <th>title</th>
                        </tr>
                        @foreach($results['topDish'] as $item )
                            <tr>
                                <th class="row">{{$item['id']}}</th>
                                <td>{{$item['total']}}</td>
                                <td>{{$item['title']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- **************END****************** -->

                <!-- **************START****************** -->
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><b>Имена официантов
                        выполнивших больше всех заказов</b></div>
                    <div class="panel-body">
                        <pre>
SELECT waiters.first_name, waiters.last_name, count(orders.waiter_id) FROM orders
	JOIN waiters ON orders.waiter_id = waiters.id
	GROUP BY orders.waiter_id
	ORDER BY orders.waiter_id;
                        </pre>
                    </div>

                    <!-- Table -->
                    <table class="table">
                        <tr>
                            <th>name</th>
                            <th>total</th>
                        </tr>
                        @foreach($results['topWaiter'] as $item )
                            <tr>
                                <td>{{$item['name']}}</td>
                                <td>{{$item['total']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- **************END****************** -->
            </div>
        </div>
    </div>
@endsection
