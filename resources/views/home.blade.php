@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Combo wombo</div>

                <div class="panel-body">
                    Добрый день!
                    <p>Реализовал все задания, кроме <i>1.3. *Создайте класс наследник, который будет использовать родительский метод для
                    получения кода и преобразовывать буквы латинского алфавита в буквы кириллического
                    алфавита не трогая цифры.</i></p>
                    <p>За основу был взят фреймворк Laravel. Для динамических запросов использовал jQuery Ajax.</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
