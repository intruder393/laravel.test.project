@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Generator</div>

                    <div id="panel-generate-code" class="panel-body">
                        <button type="button" id="button-generator" class="btn btn-primary button-update">Генерировать</button>
                        <div class="panel-code-block">
                            <p>CODE >>> {{$results['code']}}</p>
                            <p>CHAR_CODE >>> {{$results['charCode']}}</p>
                            <p>NUM_CODE >>> {{$results['numberCode']}}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Класс, позволяющий генерировать уникальные коды</div>

                    <div class="panel-body">
                        <pre>

    namespace App\myfolder;

    class CodeGenerator {

        public static $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890';
        public static $length = 10;
        public static $chars_length = 20;
        public static $gString = '';


        public static function get_rand_str() {
            //получаем длинну строки символов
            self::$chars_length = strlen(self::$chars) - 1;

            $string = self::$chars{rand(0,self::$chars_length)};
            for($i=1; $i < self::$length; $i++) {
                // Берем случайный элемент из набора символов
                $string .= self::get_rand_char($string, $i, self::$chars_length);
            }
            //echo "STRING > ".$string."<br>";
            self::$gString = $string;
            return $string;
        }

        public static function get_rand_char($string, $i, $length) {
            $rand = static::$chars{rand(0,$length)};
            if($rand != $string{$i-1}) {
                return $rand;
            } else {
                return static::get_rand_char($string, $i, $length);
            }
        }

        public static function getGuid(){
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $uuid = substr($charid, 0, 10);

            return $uuid;
        }
    }
                            </pre>
                    </div>
                </div>


                <div class="panel panel-primary">
                    <div class="panel-heading">Класс наследник, позволяющий генерировать уникальные коды только из цифр
                    </div>

                    <div class="panel-body">
                        <pre>
    namespace App\myfolder;

    class NumberGenerator extends CodeGenerator {

        public static $chars = '1234567890';
        public static $length = 10;
        public static $chars_length = 20;

        public static function get_number_code() {
            $result = '';
            $string = ( empty(parent::$gString) ) ? parent::get_rand_str() : parent::$gString;
            $numbers = preg_replace("/[^0-9]/", '', $string);

            self::$chars = ( !empty($numbers) && $numbers != '0' ) ? $numbers : self::$chars;
            self::$chars_length = strlen(self::$chars) - 1;

            for($i=0; $i < self::$length; $i++) {
                // Берем случайный элемент из набора символов
                $result .= self::$chars{rand(0,self::$chars_length)};
            }
            //echo "INT_CODE > ".$result."<br>";
            return $result;
        }
    }

                        </pre>
                    </div>
                </div>



                <div class="panel panel-primary">
                    <div class="panel-heading">Класс наследник, позволяющий генерировать уникальные коды только из букв латинского
                        алфавита
                    </div>

                    <div class="panel-body">
                        <pre>

    namespace App\myfolder;

    class CharGenerator extends CodeGenerator {

        public static $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ';
        public static $length = 10;
        public static $chars_length = 20;

        public static function get_chars_code() {
            $string = ( empty(parent::$gString) ) ? parent::get_rand_str() : parent::$gString;

            self::$chars = preg_replace("/[0-9]/", '', $string);
            self::$chars_length = strlen(self::$chars) - 1;

            $result = self::$chars{rand(0,self::$chars_length)};

            for($i=1; $i < self::$length; $i++) {
                // Берем случайный элемент из набора символов
                $result .= self::get_rand_char($result, $i, self::$chars_length);
            }
            //echo "CHAR_CODE > ".$result."<br>";
            return $result;
        }
    }
                        </pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
